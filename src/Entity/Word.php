<?php


namespace App\Entity;

use App\EntityTrait\DateTimeTrait;
use App\EntityTrait\FillTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Word
 * @package App\Entity

 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="`word`")
 *
 * @UniqueEntity(fields={"english", "russian"}, message="The word already exists")
 */
class Word
{
    use FillTrait, DateTimeTrait;

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     *
     * @Assert\NotBlank()
     */
    private $english;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     */
    private $transcription;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     *
     * @Assert\NotBlank()
     */
    private $russian;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="words")
     */
    private $users;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Training", mappedBy="word")
     */
    private $training;

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return Word
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getEnglish()//: string
    {
        return $this->english;
    }

    /**
     * @param mixed $english
     * @return Word
     */
    public function setEnglish($english): self
    {
        $this->english = $english;
        return $this;
    }

    /**
     * @return string
     */
    public function getTranscription(): string
    {
        return $this->transcription;
    }

    /**
     * @param mixed $transcription
     * @return Word
     */
    public function setTranscription($transcription): self
    {
        $this->transcription = $transcription;
        return $this;
    }

    /**
     * @return string
     */
    public function getRussian(): string
    {
        return $this->russian;
    }

    /**
     * @param mixed $russian
     * @return Word
     */
    public function setRussian($russian): self
    {
        $this->russian = $russian;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTraining()
    {
        return $this->training;
    }

    public function getTrainingWords()
    {
        return collect($this->training)->map(function (Training $training){
            return $training->getWord()->getId();
        });
    }

    /**
     * @param ArrayCollection $training
     * @return Word
     */
    public function setTraining(ArrayCollection $training): self
    {
        $this->training = $training;
        return $this;
    }

    public function getCheck()
    {
        /** @var User $user */
        $user = $this->users->current();
        return $user->isEngToRus() ? $this->getRussian() : $this->getEnglish();
    }
}