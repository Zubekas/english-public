<?php


namespace App\Entity;


use App\EntityTrait\DateTimeTrait;
use App\EntityTrait\FillTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class Training
 * @package App\Entity
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="`training`")
 *
 * @UniqueEntity(
 *     fields={"user", "word"},
 *     message="Training item already exists!"
 * )
 */
class Training
{
    use DateTimeTrait, FillTrait;

    /**
     * @var integer
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private $id;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="training", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="user", referencedColumnName="id", nullable=false)
     */
    private $user;

    /**
     * @var Word
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Word", inversedBy="training", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="word", referencedColumnName="id", nullable=false)
     */
    private $word;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_correctly", type="boolean")
     */
    private $correctly;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Training
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return Training
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Word
     */
    public function getWord(): Word
    {
        return $this->word;
    }

    /**
     * @param Word $word
     * @return Training
     */
    public function setWord(Word $word): self
    {
        $this->word = $word;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCorrectly(): bool
    {
        return $this->correctly;
    }

    /**
     * @param bool $correctly
     * @return Training
     */
    public function setCorrectly(bool $correctly): self
    {
        $this->correctly = $correctly;
        return $this;
    }

}