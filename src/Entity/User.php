<?php

namespace App\Entity;

use App\EntityTrait\SettingTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
/**
 * Class User
 * @package App\Entity
 * @ORM\Table(name="`user`")
 * @ORM\Entity
 * @UniqueEntity("username", message="The user already exists")
 * @UniqueEntity(fields={"user", "word"}, message="The word already exists")
 */
class User implements UserInterface
{
    use SettingTrait;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", options={"unsigned"=true})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, unique=true, nullable=false)
     *
     * @Assert\NotBlank()
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=180, nullable=false)
     *
     * @Assert\NotBlank()
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     *
     * @Assert\NotBlank()
     */
    private $level;

    /**
     * @var bool
     *
     * @ORM\Column(name="eng_to_rus", type="boolean", options={"default"=1})
     *
     * @Assert\Type("bool")
     */
    private $engToRus;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="App\Entity\Word", inversedBy="users")
     *
     * @ORM\JoinTable(
     *     name="user_word",
     *     joinColumns={
     *          @ORM\JoinColumn(name="user", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="word", referencedColumnName="id")
     *     }
     * )
     *
     * @Assert\NotBlank()
     */
    private $words;

    /**
     * @var PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="App\Entity\Training", mappedBy="user", orphanRemoval=true)
     */
    private $training;

    public function __construct()
    {
        $this->words     = new ArrayCollection();
    }

    /**
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param integer $id
     * @return User
     */
    public function setId($id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return User
     */
    public function setPassword(string $password, UserPasswordEncoderInterface $passwordEncoder): self
    {
        if (!empty($password)) {
            $password = $passwordEncoder->encodePassword($this, $password);
        }

        $this->password = $password;

        return $this;
    }

    /**
     * @return integer
     */
    public function getLevel(): int
    {
        return $this->level;
    }

    /**
     * @param int $level
     * @return User
     */
    public function setLevel($level): self
    {
        $this->level = $level;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEngToRus(): bool
    {
        return $this->engToRus;
    }

    /**
     * @param bool $engToRus
     * @return User
     */
    public function setEngToRus(bool $engToRus): self
    {
        $this->engToRus = $engToRus;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getWords()
    {
        return $this->words;
    }

    /**
     * @param mixed $words
     * @return User
     */
    public function setWords($words): self
    {
        if ($words instanceof ArrayCollection) {
            $this->words = $words;
        } else {
            $this->words = new ArrayCollection($words);
        }

        return $this;
    }

    /**
     * @param Word $word
     * @return User
     */
    public function addWord(Word $word): self
    {
        $this->words->add($word);
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getTraining(): PersistentCollection
    {
        return $this->training;
    }

    /**
     * @param mixed $training
     * @return User
     */
    public function setTraining($training): self
    {
        if ($training instanceof Training) {
            $this->training = $training;
        } else {
            $this->training = new ArrayCollection($training);
        }

        return $this;
    }

    /**
     * @param Training $training
     * @return User
     */
    public function addTraining(Training $training): self
    {
        $this->training->add($training);

        return $this;
    }
    /**
     * @param Training $training
     * @return User
     */
    public function removeTraining(Training $training): self
    {
        $this->training->removeElement($training);
//        $this->training->remove($training);

        return $this;
    }

    /**
     * Returns the roles granted to the user.
     * @return array (Role|string)[] The user roles
     */
    public function getRoles()
    {
        return [
            'ROLE_USER'
        ];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return User
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials()
    {
        // TODO: Implement eraseCredentials() method.
    }
}