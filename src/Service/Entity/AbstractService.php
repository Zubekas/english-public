<?php

namespace App\Service\Entity;

use App\Entity\Traits\Fill\FillTrait;
use App\Exception\Exception;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractService
{
    /** @var EntityManagerInterface  */
    protected $entityManager;

    /** @var EntityRepository */
    protected $repository;

    /** @var ValidatorInterface  */
    protected $validator;

    abstract protected function getClass();

    /**
     * AbstractService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param ValidatorInterface $validator
     */
    public function __construct(EntityManagerInterface $entityManager, ValidatorInterface $validator)
    {
        $this->entityManager = $entityManager;
        $this->repository    = $entityManager->getRepository($this->getClass());

        $this->validator = $validator;
    }

    /**
     * @param array $data
     * @return mixed
     * @throws Exception
     */
    public function createEntity(array $data)
    {
        $entityClassName = $this->getClass();
        /** @uses FillTrait::fill() */
        $entity = $entityClassName::fill($data, $this->entityManager);

        $errors = $this->validator->validate($entity);

        if ($errors->count()) {
            throw new Exception($errors);
        }

        $this->entityManager->persist($entity);

        return $entity;
    }

    /**
     * @param array $data
     * @return object|null
     * @throws Exception
     */
    public function createOrUpdateIfExists(array $data)
    {
        $className = $this->getClass();

        $entityMeta = $this->entityManager->getMetadataFactory()->getMetadataFor($this->getClass());
        $identifierFieldNames = $entityMeta->getIdentifierFieldNames();
        if (empty($identifierFieldNames)) {
            throw new Exception(sprintf("Not found identifierFieldNames for class '%s'", $this->getClass()));
        }

        $identifierCriteria = collect($data)
            ->only($identifierFieldNames)
            ->toArray();
        if (count($identifierFieldNames) != count($identifierCriteria)) {
            throw new Exception(sprintf("Incorrect identifier criteria"));
        }

        $entity = $this->getOneBy($identifierCriteria);
        if (!$entity instanceof $className) {
            $entity = new $className();
            $isNeedPersist = true;
        }

        foreach ($data as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (!method_exists($entity, $method)) {
                continue;
            }

            $entity->{$method}($value);
        }

        if ($isNeedPersist ?? false) {
            $this->entityManager->persist($entity);
        }

        return $entity;
    }

    /**
     * @param array $criteria
     * @return null|object
     */
    public function getOneBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param array $criteria
     * @param array|null $orderBy
     * @param null $limit
     * @param null $offset
     * @return array|object[]
     */
    public function getBy(array $criteria, array $orderBy = [], $limit = null, $offset = null)
    {
        return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
    }

    /**
     * @param Criteria $criteria
     * @return \Doctrine\Common\Collections\Collection
     */
    protected function getByCriteria(Criteria $criteria)
    {
        return $this->repository->matching($criteria);
    }

    /**
     * @param int $id
     * @return null|object
     */
    public function getById(int $id)
    {
        return $this->repository->find($id);
    }

    /**
     * @param array $ids
     * @return array|object[]
     */
    public function getByIds(array $ids)
    {
        return $this->repository->findBy(['id' => $ids]);
    }

    /**
     * @param null $entity
     * @param bool $isFlush
     */
    public function save($entity = null, $isFlush = true)
    {
        if (null !== $entity) {
            $this->entityManager->persist($entity);
        }
        if (true === $isFlush) {
            $this->entityManager->flush();
        }
    }

    /**
     * @param $entity
     * @param bool $isFlush
     */
    public function remove($entity, $isFlush = true)
    {
        $this->entityManager->remove($entity);
        if (true === $isFlush) {
            $this->entityManager->flush();
        }
    }

    public function clear()
    {
        $this->entityManager->clear($this->getClass());
    }

    /**
     * @param array $criteria
     * @return int
     */
    public function getCount(array $criteria = [])
    {
        return $this->repository->count($criteria);
    }

    /**
     * @param Criteria $criteria
     * @return int
     */
    protected function getCountByCriteria(Criteria $criteria)
    {
        return $this->repository->matching($criteria)->count();
    }

    /**
     * @return null|object
     */
    public function getAll()
    {
        return $this->repository->findAll();
    }
}
