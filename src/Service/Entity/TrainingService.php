<?php


namespace App\Service\Entity;


use App\Entity\Training;

class TrainingService extends AbstractService
{

    protected function getClass()
    {
        return Training::class;
    }

    public function clear()
    {
        parent::clear();
        $this->entityManager->flush();
    }
}