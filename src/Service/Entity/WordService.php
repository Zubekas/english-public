<?php


namespace App\Service\Entity;


use App\Entity\User;
use App\Entity\Word;
use App\Exception\Exception;
use App\Service\ErrorService;

class WordService extends AbstractService
{
    protected function getClass()
    {
        return Word::class;
    }

    /**
     * @param array $data
     * @param User $user
     * @return mixed
     * @throws \App\Exception\Exception
     */
    public function create(array $data, User $user)
    {
        if (!$word = $data['word']) {
            $word = $this->createEntity($data);
        }
        if (!$word instanceof Word) {
            throw new Exception(ErrorService::WORD_NOT_FOUND);
        }
        if ($user->getWords()->contains($word)) {
            throw new Exception(ErrorService::WORD_ALREADY_EXIST);
        }
        $user->addWord($word);

        $this->entityManager->flush();
    }


}