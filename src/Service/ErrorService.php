<?php


namespace App\Service;


class ErrorService
{
    const
        USER_NOT_FOUND          = 'USER_NOT_FOUND',

        WORD_NOT_FOUND          = 'WORD_NOT_FOUND',
        WORD_ALREADY_EXIST      = 'WORD_ALREADY_EXIST'
    ;
    public static $errorTexts = [
        self::USER_NOT_FOUND            => 'Пользователь не найден',

        self::WORD_NOT_FOUND            => 'Слово не найдено',
        self::WORD_ALREADY_EXIST        => 'Слово уже существует'
    ];
}