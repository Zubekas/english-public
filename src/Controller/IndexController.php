<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Word;
use App\Exception\Exception;
use App\Form\AppendWordType;
use App\Form\CheckWordType;
use App\Service\Entity\TrainingService;
use App\Service\ErrorService;
use App\Service\Entity\WordService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 * @package App\Controller
 *
 * @Route(
 *     "/"
 * )
 */
class IndexController extends AbstractController
{
    /** @var WordService $wordService */
    private $wordService;
    /** @var TrainingService $trainingService */
    private $trainingService;

    public function __construct(WordService $wordService, TrainingService $trainingService)
    {
        $this->wordService      = $wordService;
        $this->trainingService  = $trainingService;
    }

    /**
     * @Route("/", methods={"GET", "POST"}, name="english")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        if (!$user instanceof User) {
            throw new Exception(ErrorService::USER_NOT_FOUND);
        }

        $words = $user->getWords()->toArray();

        if ($words == []) {
            return $this->redirectToRoute('append');
        }

        do {
            if ($words == []) {
                return $this->redirectToRoute('report');
            }
            shuffle($words);
            /** @var Word $word */
            $word = array_shift($words);
        } while ($word->getTrainingWords()->contains($word));

        $form = $this->createForm(CheckWordType::class, null, ['word' => $word->getId()]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data               = $form->getData();
            $word               = $this->wordService->getById($data['word']);
            $data['user']       = $user;
            $data['correctly']  = trim($data['checkWord']) == $word->getCheck();
            $this->trainingService->createEntity($data);
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('english');
        }

        return $this->render('index/index.html.twig', [
            'form' => $form->createView(),
            'word' => $word,
            'user' => $user
        ]);
    }

    /**
     * @Route("/append", methods={"GET", "POST"}, name="append")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function append(Request $request)
    {
        $form = $this->createForm(AppendWordType::class);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $this->getUser();

            $this->wordService->create($data, $user);

            return $this->redirectToRoute('english');
        }

        return $this->render('index/append.html.twig', [
            'form' => $form->createView()
        ]);
    }
}