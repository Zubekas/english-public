<?php


namespace App\Controller;


use App\Entity\User;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class IndexController
 * @package App\Controller
 *
 * @Route(
 *     "/report", name="report"
 * )
 */
class ReportController extends AbstractController
{
    /**
     * @Route("/", methods={"GET", "POST"}, name="")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(Request $request)
    {
        return $this->render('report/index.html.twig', [

        ]);
    }

    /**
     * @Route("/clear", methods={"DELETE"}, name="_clear")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clear(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        foreach ($user->getTraining() as $training) {
            $user->removeTraining($training);
        }
//        $this->getDoctrine()->getManager()->persist($user);
        $this->getDoctrine()->getManager()->flush();


        return $this->json(['error' => null]);
    }
}