<?php


namespace App\Controller;

use App\Entity\User;
use App\Form\CheckWordType;
use App\Form\SettingsType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class SettingsController
 * @package App\Controller
 *
 * @Route(
 *     "/settings"
 * )
 */
class SettingsController extends AbstractController
{
    /**
     * @Route("/", methods={"GET", "POST"}, name="settings")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws Exception
     */
    public function index(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(SettingsType::class, null, ['user' => $user]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEngToRus(boolval($form->getData()['engToRus']));
            $this->getDoctrine()->getManager()->flush();
        }

        return $this->render('setting/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/", methods={"DELETE"}, name="clear")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function clear(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $user->getTraining()->clear();
        return $this->json(['error' => $user->getTraining()]);
    }
}