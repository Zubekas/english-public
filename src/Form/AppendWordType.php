<?php


namespace App\Form;


use App\Entity\Word;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AppendWordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('word', EntityType::class, [
                'label'         => 'Выбрать из списка',
                'placeholder'   => 'Выбрать',
                'class'         => Word::class,
                'empty_data'    => null,
                'required'      => false,
                'choice_label'  => function (Word $word) {
                    /** @var Word $word */
                    return $word->getEnglish() . ' - ' . $word->getRussian();
                },
                'attr'          => [
                    'class' => 'chosen-select'
                ]
            ])
            ->add('switch', HiddenType::class, [
                'data'      => 1,
                'attr'      => [
                    'class' => 'switch'
                ]
            ])
            ->add('english', TextType::class, [
                'label'     => 'Слово на английском',
                'required'  => false,
            ])
            ->add('transcription', TextType::class, [
                'label'     => 'Транскрипция',
                'required'  => false
            ])
            ->add('russian', TextType::class, [
                'label'     => 'Перевод на русском',
                'required'  => false,
            ])
            ->add('save', SubmitType::class, [
                'label'     => 'Сохранить'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => [
                'class' => 'form-with-switch'
            ]
        ]);
    }
}