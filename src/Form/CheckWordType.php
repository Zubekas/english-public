<?php


namespace App\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CheckWordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** Word $word */
        $word = $options['word'];

        $builder
            ->add('checkWord', TextType::class, [
                'label' => ' ',
                'attr'  => [
                    'class' => 'col-auto text-center form-group'
                ]
            ])
            ->add('word', HiddenType::class, [
                'data' => $word,
            ])
            ->add('check', SubmitType::class, [
                'attr'  => [
                    'class' => 'col-auto text-center form-group btn btn-outline-success btn-lg btn-block'
                ]
            ])
            ;
    }
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'word' => null,
            'attr' => [
                'class' => 'justify-content-md-center',
            ],
        ]);
    }
}