<?php

namespace App\Exception;

use App\Service\ErrorService;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Throwable;

class Exception extends \Exception
{
    /**
     * Exception constructor.
     * @param string | ConstraintViolationListInterface $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message, int $code = 0, Throwable $previous = null)
    {
        if ($message instanceof ConstraintViolationListInterface) {
            $message = $this->toString($message);

        }
        parent::__construct($message, $code, $previous);
    }

    /**
     * @param ConstraintViolationListInterface $error
     * @return string
     */
    public function toString(ConstraintViolationListInterface $error): string
    {
        if ($error->count() == 1) {
            $response = [
                'error'         => $error[0]->getCode() ?: $error[0]->getMessage(),
                'errorMessage'  => $error[0]->getMessage(),
                'errorPath'     => $error[0]->getPropertyPath(),
                'errorValue'    => $error[0]->getInvalidValue(),
            ];
        } else {
            $response = [
                'error'         => ErrorService::MULTIPLE_ERROR,
                'errorMessage'  => ErrorService::$errorTexts[ErrorService::MULTIPLE_ERROR],
                'errorList'     => []
            ];
            /** @var ConstraintViolation $violation */
            foreach ($error as $violation) {
                $response['errorList'][$violation->getPropertyPath()] = [
                    'error'        => $violation->getCode() ?: $violation->getMessage(),
                    'errorMessage' => $violation->getMessage(),
                    'errorValue'   => $violation->getInvalidValue(),
                ];
            }
        }

        return json_encode($response);
    }
}
