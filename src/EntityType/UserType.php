<?php


namespace App\EntityType;


interface UserType
{
    const
        SETTING_RUS_TO_ENG = 0,
        SETTING_ENG_TO_RUS = 1
    ;
}