<?php


namespace App\EntityTrait;


use App\EntityType\UserType;

trait SettingTrait
{
    public function getOptionEngToRus()
    {
        return [
            UserType::SETTING_RUS_TO_ENG => 'Проверка Русский => English',
            UserType::SETTING_ENG_TO_RUS => 'Проверка English => Русский',
        ];
    }
}