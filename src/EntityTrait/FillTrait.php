<?php

namespace App\EntityTrait;

use App\Exception\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;

trait FillTrait
{
    /**
     * @param array $data
     * @param EntityManagerInterface $entityManager
     *
     * @return self
     * @throws Exception
     * @throws ORMException
     */
    public static function fill(array $data, EntityManagerInterface $entityManager)
    {
        $entity = new self();

        $entityMeta         = $entityManager->getMetadataFactory()->getMetadataFor(get_class($entity));
        $relatedEntityList  = $entityMeta->getAssociationNames();

        foreach ($data as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (!method_exists($entity, $method)) {
                continue;
            }

            if (in_array($key, $relatedEntityList) && !is_object($value) && !is_array($value)) {
                if (!is_numeric($value)) {
                    throw new Exception(sprintf("'%s' must be numeric", $key));
                }
                $value = $entityManager->getReference($entityMeta->getAssociationMapping($key)['targetEntity'], (int) $value);
            }

            if (in_array($entityMeta->getTypeOfField($key), ['datetime', 'date'])) {
                if (empty($value)) {
                    continue;
                }
                $timestamp = strtotime($value);
                if (false === $timestamp) {
                    throw new Exception(sprintf("'%s' is not datetime string", $value));
                }
                $value = (new \DateTime())->setTimestamp($timestamp);
            }

            $entity->{$method}($value);
        }

        return $entity;
    }
}