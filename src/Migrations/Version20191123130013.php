<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191123130013 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `word` (id INT AUTO_INCREMENT NOT NULL, english VARCHAR(255) NOT NULL, transcription VARCHAR(255) DEFAULT NULL, russian VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX UNIQ_C3F175112C830200 (english), UNIQUE INDEX UNIQ_C3F17511329CE984 (transcription), UNIQUE INDEX UNIQ_C3F17511EB66F175 (russian), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `training` (id INT AUTO_INCREMENT NOT NULL, user INT UNSIGNED NOT NULL, word INT NOT NULL, is_correctly TINYINT(1) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME NOT NULL, INDEX IDX_D5128A8F8D93D649 (user), INDEX IDX_D5128A8FC3F17511 (word), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE `user` (id INT UNSIGNED AUTO_INCREMENT NOT NULL, login VARCHAR(180) NOT NULL, password VARCHAR(180) NOT NULL, name VARCHAR(255) DEFAULT NULL, level INT NOT NULL, UNIQUE INDEX UNIQ_8D93D649AA08CB10 (login), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_word (user INT UNSIGNED NOT NULL, word INT NOT NULL, INDEX IDX_B97039D88D93D649 (user), INDEX IDX_B97039D8C3F17511 (word), PRIMARY KEY(user, word)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `training` ADD CONSTRAINT FK_D5128A8F8D93D649 FOREIGN KEY (user) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE `training` ADD CONSTRAINT FK_D5128A8FC3F17511 FOREIGN KEY (word) REFERENCES `word` (id)');
        $this->addSql('ALTER TABLE user_word ADD CONSTRAINT FK_B97039D88D93D649 FOREIGN KEY (user) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE user_word ADD CONSTRAINT FK_B97039D8C3F17511 FOREIGN KEY (word) REFERENCES `word` (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `training` DROP FOREIGN KEY FK_D5128A8FC3F17511');
        $this->addSql('ALTER TABLE user_word DROP FOREIGN KEY FK_B97039D8C3F17511');
        $this->addSql('ALTER TABLE `training` DROP FOREIGN KEY FK_D5128A8F8D93D649');
        $this->addSql('ALTER TABLE user_word DROP FOREIGN KEY FK_B97039D88D93D649');
        $this->addSql('DROP TABLE `word`');
        $this->addSql('DROP TABLE `training`');
        $this->addSql('DROP TABLE `user`');
        $this->addSql('DROP TABLE user_word');
    }
}
