window.App = {};
require('../css/app.css');
require('bootstrap');
window.$ = window.jQuery = require('jquery');

require('chosen-js');

$(document).ready(function () {
    $('.chosen-select').chosen();

   $('#accordion').on('click', function () {
       var $form        = $('.form-with-switch'),
           $switch      = $form.find('.switch'),
           $collapseOne = $(this).find('#collapseOne'),
           $collapseTwo = $(this).find('#collapseTwo');
       setTimeout(function () {
           if ($collapseOne.hasClass('show')) {
               $switch.val(1);
           } else if ($collapseTwo.hasClass('show')) {
               $switch.val(2);
           }
       }, 500)
   });

   $('#method-delete').on('click', function () {
       var $this = $(this),
           url   = $this.data('url');
       console.log(url);
       $.ajax({
           type: "delete",
           dataType: 'json',
           url: url,
           success: function(data){
               console.log(data);
           },
           error: function(error){
               // var error = error.responseJSON;
               console.log('error');
               console.log(error);
           }
       });
   });
});